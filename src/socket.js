
import socketIOClient from "socket.io-client";
const ENDPOINT = "http://127.0.0.1:3001";

let socket;

class Socket {
  static Open() {
    Socket.Close();
    socket = socketIOClient(ENDPOINT);
  }

  static Close() {
    socket && socket.close();
  }

  static Get() {
    return socket;
  }
}


export default Socket;
