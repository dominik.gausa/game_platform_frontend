import React, { useState, useEffect } from "react";
import './App.css';
import Socket from './socket'
import GameList from './Game/GameList'
import GameCatch from './Game/GameCatch'
import GameTicTacToe from './Game/GameTicTacToe'
import Game4wins from './Game/Game4wins'
import { v4 as uuidv4 } from 'uuid';
import { BrowserRouter, Route, Link } from 'react-router-dom'



class App extends React.Component {
  state = {
    state: 'auth',
    currentGames: [],
    gameType: 'catch'
  }

  constructor(props) {
    super(props)

    this.UpdateRoomlist = this.UpdateRoomlist.bind(this)
    this.handleAuth = this.handleAuth.bind(this)
    this.handleCreateGame = this.handleCreateGame.bind(this)
    this.handleJoinGame = this.handleJoinGame.bind(this)
  }

  componentDidMount() {
    Socket.Open()
    Socket.Get().on('ping', data => {
      console.debug(data)
    });
    if(localStorage.getItem('username') && localStorage.getItem('uuid')) {
      this.state.username = localStorage.getItem('username');
      this.state.uuid = localStorage.getItem('uuid');
      this.handleAuth(); 
    }else{
      localStorage.setItem('uuid', uuidv4());
      Socket.Get().emit('ping', 'hi');
    }
  }

  componentWillUnmount() {
    Socket.Close()
  }

  UpdateRoomlist() {
    Socket.Get().once('room', (repl) => {
      console.debug(repl);
      this.setState({
        state: 'listGames',
        currentGames: repl.rooms
      })
    });
    Socket.Get().emit('room', {request: 'list'})
  }

  handleCreateGame(e) {
    e.preventDefault();
    if(!this.state.roomSelected || this.state.roomSelected == ""){
      alert("room needs a name")
      return;
    }

    let createRoom = {room: this.state.roomSelected, request: 'create', type: this.state.gameType}
    console.debug(createRoom)
    Socket.Get().emit('room',  createRoom)
    this.setState({
      state: 'play'
    })
  }

  handleJoinGame(game) {
    this.state.roomSelected = game.room
    let createRoom = {room: this.state.roomSelected, request: 'join'}
    console.debug(createRoom)
    Socket.Get().emit('room',  createRoom)
    this.setState({
      state: 'play',
      gameType: game.type
    })
  }

  handleAuth(e) {
    if(e) e.preventDefault();
    
    if(!this.state.username || this.state.username == ""){
      alert("You need a name")
      return;
    }
    localStorage.setItem('username', this.state.username);

    console.log(`Final username: ${this.state.username}`);
    Socket.Get().once('auth', (reply) => {
      this.UpdateRoomlist();
    });
    Socket.Get().emit('auth', {
      user: this.state.username,
      pass: 'Quark',
      uuid: this.state.uuid
    });
  }

  render() {
    <BrowserRouter>
      
    </BrowserRouter>
    switch(this.state.state) {
      case 'play':
        switch(this.state.gameType) {
          case 'catch':
            return(
            <div>
              Your name: {this.state.username}    |  Room name: {this.state.roomSelected}
              <hr />
              <GameCatch room={this.state.roomSelected}/>
            </div>)
            break;
          case 'TicTacToe':
            return(
            <div>
              Your name: {this.state.username}    |  Room name: {this.state.roomSelected}
              <hr />
              <GameTicTacToe room={this.state.roomSelected}/>
            </div>)
            break;
          case '4wins':
            return(
            <div>
              Your name: {this.state.username}    |  Room name: {this.state.roomSelected}
              <hr />
              <Game4wins room={this.state.roomSelected}/>
            </div>)
            break;
        }

      case 'listGames':
        return (
          <div>
            Your name: {this.state.username}
            <hr />
            <form onSubmit={this.handleCreateGame}>
              Roomname:<input id="name" type="text" onChange={(e) => this.state.roomSelected = e.target.value}/>
              <select value={this.state.gameType} onChange={(e) => {
                this.setState({gameType: e.target.value})
              }}>
                <option value="catch">catch</option>
                <option value="TicTacToe">TicTacToe</option>
                <option value="4wins">4wins</option>
              </select>
              <input type="submit" value="Submit" />
            </form>
            <hr />
            <div onClick={this.UpdateRoomlist}>UpdateList</div>
            <GameList games={this.state.currentGames || []} join={this.handleJoinGame} />
          </div>
        );

      case 'auth':
      default:
        return (
          <div>
            <form onSubmit={this.handleAuth}>
              Username:<input id="name" type="text" onChange={(e) => this.state.username = e.target.value}/> 
            </form>
          </div>
        );
    }
  }
}

export default App;
