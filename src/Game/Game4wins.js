import React, { useState, useEffect } from "react";
import Socket from '../socket'
import style from './style.css'


function Field(param, key) {
  const styleByState = {
    X: 'green',
    O: 'red'
  };
  return <div key={key} className={`fourwinsField ${styleByState[param.state] ? styleByState[param.state] : ''}`} onClick={() => param.click()}>{param.state}</div>
}

class Game4wins extends React.Component {
  state = {
    lastTick: {},
    isActive: false,
    field: [
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.']
    ]
  }

  constructor(props) {
    super(props)

    this.handleGamedata = this.handleGamedata.bind(this)
    this.handleMove = this.handleMove.bind(this)
    this.handleReset = this.handleReset.bind(this)
  }
  
  componentDidMount() {
    Socket.Get().on('game', this.handleGamedata);
  }
  
  componentWillUnmount() {
    Socket.Get().removeAllListeners('game')
  }

  handleGamedata(data) {
    console.debug({currentGameState: data})
    switch(data.type) {
      case 'tick': 
        this.setState({
          lastTick: data,
          field: data.field,
          isActive: data.activePlayer == localStorage.getItem('uuid')
        });
        break;

      case 'info':
        this.setState({isActive: data.msg == 'youre it'})
        break;
    }
  }

  handleReset() {
    Socket.Get().emit('room', {
      room: this.props.room,
      request: 'action',
      data: 'reset'
    });
    this.setState({isActive: false})
  }

  handleMove(fieldIdx) {
    Socket.Get().emit('room', {
      room: this.props.room,
      request: 'action',
      data: fieldIdx
    });
  }

  RenderFiled(num) {
    return Field({
      click: () => this.handleMove(num%this.state.field[0].length),
      state: this.state.field[Math.floor(num/this.state.field[0].length)][num%this.state.field[0].length]
    }, num);
  }

  render() {
    if(!this.state.field) {
      return(<div>...</div>)
    }
    return (
      <div>
        {JSON.stringify(this.state.lastTick)}
        <div onClick={() => this.handleReset()}>Reset</div>
        {this.state.isActive ? (<div>active</div>):(<div></div>)}
        <br />
        <div className="fourwins-container">
          {this.state.field.map((y, yIdx) =>
              y.map((x, xIdx) =>
                this.RenderFiled(yIdx*this.state.field[0].length + xIdx, x)))}
        </div>
      </div>
    )
  }
}

export default Game4wins;
