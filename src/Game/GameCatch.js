import React, { useState, useEffect } from "react";
import Socket from '../socket'

class GameCatch extends React.Component {
  state = {
    lastTick: {},
    isActive: false
  }

  constructor(props) {
    super(props)

    this.handlePassOver = this.handlePassOver.bind(this)
    this.handleGamedata = this.handleGamedata.bind(this)
  }
  
  componentDidMount() {
    Socket.Get().on('game', this.handleGamedata);
  }
  
  componentWillUnmount() {
    Socket.Get().removeAllListeners('game')
  }

  handleGamedata(data) {
    console.debug({currentGameState: data})
    switch(data.type) {
      case 'tick': 
        this.setState({lastTick: data});
        break;

      case 'info':
        this.setState({isActive: data.msg == 'youre it'})
        break;
    }
  }

  handlePassOver() {
    Socket.Get().emit('room', {
      room: this.props.room,
      request: 'action',
      data: 'pass'
    });
    this.setState({isActive: false})
  }

  render() {
    return (
      <div>
        {JSON.stringify(this.state.lastTick)}
        {this.state.isActive ? (<div onClick={this.handlePassOver}>active</div>):(<div></div>)}
      </div>
    )
  }
}

export default GameCatch;
