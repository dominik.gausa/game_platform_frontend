import React, { useState, useEffect } from "react";
import Socket from '../socket'

function GameField () {
  const [response, setResponse] = useState("");

  function handleClick(e) {
    e.preventDefault();
    let pos = {
      x: e.clientX - e.target.offsetLeft,
      y: e.clientY - e.target.offsetTop
    };
    
    Socket.Get().emit('ping', pos)
    console.log(`Position: ${pos.x} ${pos.y}`)
    setResponse(JSON.stringify(pos))
  }

  return (
    <div>
      <p>Clicked position: {response}</p>
      <div style={{height: 100, width: 100, backgroundColor: "#4FF"}} onClick={handleClick}>

      </div>
    </div>
  );
}

export default GameField;
