import React, { useState, useEffect } from "react";

function GameList(props) {
  console.debug({games: props.games})
  return (
    <table>
      {props.games.map((g, idx) => {
        return (<tr key={idx}><td>{g.room}</td><td>{g.type}</td><td><div onClick={() => props.join(g)}>JOIN</div></td></tr>);
      })}
    </table>
  );
}

export default GameList;
